import os


os.environ.setdefault('DJANGO_ENVIROMENT_SETTINGS', 'local_dev')
ENV = os.environ['DJANGO_ENVIROMENT_SETTINGS']

if ENV == 'local_dev':
    # Только в случае локальных настроек, загружаем .env-файл напрямую
    # В случае настроек для docker (docker-dev или docker-prod),
    # за подгрузку переменных окружения отвечает docker-compose
    import envvars
    # Пришлось продублировать BASE_DIR,
    # из-за отсуствия возможности импортировать его из .common.base
    # без проставления переменных окружения
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    envvars.load(os.path.join(BASE_DIR, '.env'))

# Импорт общих настроек
from .common import *

# Импорт настроек, специфичных для окружения
if ENV == 'docker_dev':
    from .enviroment.docker_dev import *
elif ENV == 'docker_prod':
    from .enviroment.docker_prod import *
else:
    from .enviroment.local_dev import *

# Импорт локальных настроек (см. local.example.py)
try:
    from .enviroment.local import *
except:
    pass


if DEBUG:
    # Профилирование с помощью silk
    INSTALLED_APPS.append('silk')
    MIDDLEWARE.insert(4, 'silk.middleware.SilkyMiddleware')  # После всяких базовых middleware

    # Блок настроек debug_toolbar
    DEBUG_TOOLBAR_PATCH_SETTINGS = False
    INTERNAL_IPS = ['127.0.0.1', ]
    # Если вставить до django.contrib.staticfile, то будет ошибка
    INSTALLED_APPS.insert(0, 'debug_toolbar')
    # Если вставить это в конец middleware, то будет ошибка. Хз поч
    MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')
