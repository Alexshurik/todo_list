import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

DEBUG = False

ROOT_URLCONF = 'todo_list.urls'

WSGI_APPLICATION = 'todo_list.wsgi.application'

ADMINS = [
    ('todo_list', 'todo_list@mail.ru'),
]

MEDIA_URL = '/media/'
MEDIA_ROOT = '/var/todo_list/media/'

DATA_UPLOAD_MAX_MEMORY_SIZE = None

STATIC_URL = '/static/'
STATIC_ROOT = '/var/todo_list/static/'

SECRET_KEY = "don't use me in production))"
