ALLOWED_HOSTS = [
    '*'
]

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'todo_list',
        'USER': 'todo_list',
        'PASSWORD': 'todo_list',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
BROKER_URL = 'amqp://localhost'

REDIS_URL = 'redis://127.0.0.1:6379/0'

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/0',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'IGNORE_EXCEPTIONS': True,  # mimics memcache behavior.
                                        # http://niwinz.github.io/django-redis/latest/#_memcached_exceptions_behavior
        }
    }
}
