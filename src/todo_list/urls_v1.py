from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token


app_name = 'api-v1'
urlpatterns = [
    url(r'^login/$', obtain_jwt_token, name='login'),
    url(r'', include('tasks.urls')),
]
