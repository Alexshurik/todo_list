from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'api/v1/', include('todo_list.urls_v1', namespace='api-v1')),
]


if settings.DEBUG:
    # Профилирование запросов
    # По мне, silk привносит слишком большой overhead для того, чтобы его юзать в prod-е
    # Возможно, можно его как-то оптимизировать
    urlpatterns += [url(r'^silk/', include('silk.urls', namespace='silk'))]

    # Подключаем django_debug_toolbar для админки
    import debug_toolbar
    urlpatterns += url(r'^__debug__/', include(debug_toolbar.urls)),

    # Подключаем раздачу статик файлов для dev версии
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
