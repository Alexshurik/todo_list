import tempfile


def get_drf_formatted_date(date):
    """
    Возвращает дату в DRF-формате
    """
    if not date:
        return None

    value = date.isoformat()
    if value.endswith('+00:00'):
        value = value[:-6] + 'Z'
    return value


def get_test_file():
    """
    Возвращает тестовый файл
    """
    file = tempfile.NamedTemporaryFile(mode='w+', delete=False)
    # Для того, чтобы файл был не пустым, записываю туда любой текст
    with file as _file:
        _file.write('whatever')
    return file


def get_objects_ids(objects):
    return [object['id'] for object in objects]
