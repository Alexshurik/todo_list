from django.contrib.auth import get_user_model
from jwt import exceptions
from rest_framework_jwt.settings import api_settings


User = get_user_model()

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER


def get_jwt_token(user):
    """
    Generate JWT token for user to use it in tests
    :param user: User model object
    :return: JWT token
    """
    payload = jwt_payload_handler(user)
    return jwt_encode_handler(payload)


def get_user_by_token(token):
    """
    Retrieve user by his JWT token
    :param token: user JWT token
    :return: user object
    """
    try:
        payload = jwt_decode_handler(token)
    except (exceptions.ExpiredSignature, exceptions.DecodeError):
        return None

    user_id = payload.get('user_id')
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return None
    return user


def get_jwt_token_from_request(request):
    """
    Retrieve token from request object
    Extracts token from cookies or HTTP_AUTHORIZATION header
    :param request: Django request object
    :return: token
    """
    token = None
    if 'auth_token' in request.COOKIES:
        token = request.COOKIES['auth_token']
    elif 'HTTP_AUTHORIZATION' in request.META \
            and request.META['HTTP_AUTHORIZATION'].startswith('JWT'):
        split = request.META['HTTP_AUTHORIZATION'].split()
        if len(split) == 2:
            token = split[1]
    return token
