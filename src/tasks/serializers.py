from rest_framework import serializers

from tasks.models import Task


class TaskSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.pk')

    class Meta:
        model = Task
        fields = (
            'id',
            'name',
            'author',
            'creation_datetime',
            'due_datetime',
            'done',
        )
