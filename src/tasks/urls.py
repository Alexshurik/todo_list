from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from tasks.views import TaskViewSet


router = routers.SimpleRouter()
router.register(r'tasks', TaskViewSet, base_name='task')


urlpatterns = [
    url(r'^', include(router.urls)),
]
