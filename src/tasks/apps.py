from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SillsConfig(AppConfig):
    name = 'tasks'
    verbose_name = _('Задания')
