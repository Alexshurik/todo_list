from unittest import mock

from django.contrib.auth import get_user_model
from django.utils import timezone
from model_mommy import mommy
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from common.utils.tests import get_drf_formatted_date, get_objects_ids
from tasks.models import Task

from src.common.utils.jwt import get_user_by_token

User = get_user_model()


class SillsAPITests(APITestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        cls.user = mommy.make(User)

    def setUp(self):
        self.client.force_authenticate(user=self.user)

    def test_create_task(self):
        url = reverse('api-v1:task-list')
        data = {
            'name': 'My task',
            'due_datetime': get_drf_formatted_date(timezone.now()),
        }
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(
            Task.objects.filter(name='My task', author=self.user).exists()
        )

    def test_retrieve_my_task(self):
        task = mommy.make(Task, author=self.user)
        url = reverse('api-v1:task-detail', args=[task.pk])

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_others_task(self):
        some_user = mommy.make(User)
        task = mommy.make(Task, author=some_user)
        url = reverse('api-v1:task-detail', args=[task.pk])

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_list_tasks(self):
        url = reverse('api-v1:task-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_my_task(self):
        task = mommy.make(Task, author=self.user)

        url = reverse('api-v1:task-detail', args=[task.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_other_task(self):
        some_user = mommy.make(User)
        task = mommy.make(Task, author=some_user)

        url = reverse('api-v1:task-detail', args=[task.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_filter_tasks_by_done(self):
        # Должны найтись по запросы ?done=true
        task1 = mommy.make(Task, author=self.user, done=True)
        task2 = mommy.make(Task, author=self.user, done=True)
        task3 = mommy.make(Task, author=self.user, done=True)

        # Не должны найтись
        mommy.make(Task)
        mommy.make(Task)

        url = reverse('api-v1:task-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        retrieved_ids = get_objects_ids(response.data)
        self.assertCountEqual(
            retrieved_ids,
            [task1.pk, task2.pk, task3.pk]
        )

    @mock.patch('tasks.views.some_heavy_task_processing.delay')
    def test_celery_task_user_not_exists(self, mocked_cache):
        task = mommy.make(Task, author=self.user)
        url = reverse('api-v1:task-process', args=[task.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(mocked_cache.set.call_count, 0)

    def test_login(self):
        # На всякие случае добавил этот тест в это приложение,
        # по сути, это не надо тестировтаь
        self.client.force_authenticate()

        user = mommy.make(User, username='someuser')
        user.set_password('0112358q')
        user.save()

        url = reverse('api-v1:login')
        data = {
            'username': 'someuser',
            'password': '0112358q'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            get_user_by_token(response.data['token']),
            user
        )
