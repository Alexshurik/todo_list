from django.contrib import admin

from tasks.models import Task


@admin.register(Task)
class TakAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'name',
        'author',
        'creation_datetime',
        'done',
    )
    list_filter = ('done',)
    search_fields = ('name',)
    readonly_fields = ('creation_datetime',)
