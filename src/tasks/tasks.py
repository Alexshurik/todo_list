from common.utils.models import get_object_or_none
from tasks.models import Task
from todo_list.celery import app


@app.task
def some_heavy_task_processing(task_id):
    task = get_object_or_none(Task, pk=task_id)
    if not task:
        return

    print('Hello world!!')
    # Здесь какая-то тяжелая операция
