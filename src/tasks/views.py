from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from tasks.models import Task
from tasks.serializers import TaskSerializer
from tasks.tasks import some_heavy_task_processing


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    filter_fields = ('done',)

    def get_queryset(self):
        return super().get_queryset().filter(author=self.request.user)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @action(detail=True)
    def process(self, request, pk):
        task = self.get_object()
        some_heavy_task_processing.delay(task.pk)
        return Response()
