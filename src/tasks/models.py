from django.contrib.auth import get_user_model
from django.db import models


User = get_user_model()


class Task(models.Model):
    author = models.ForeignKey(
        User,
        verbose_name='Автор задания',
        on_delete=models.CASCADE
    )
    name = models.CharField('Название', max_length=100)
    creation_datetime = models.DateTimeField('Дата создания', auto_now_add=True)
    due_datetime = models.DateTimeField('Время окончания')
    done = models.BooleanField('Выполнено', default=False)

    class Meta:
        verbose_name = 'Задание'
        verbose_name_plural = 'Задания'

    def __str__(self):
        return self.name
