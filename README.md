# Тестовый проект для примера структуры проектов на бэке

## Запуск:

Создать файл **src/.env**

Примеры файла:

src/env.example

**Запустить докер-композ (локальная версия):**
```
docker-compose -f docker-compose-dev.yml up --build
```

**Запустить докер-композ (production версия):**
```
docker-compose -f docker-compose-dev.yml up --build -d
```

## Доступ в Django-админку

**Создать админа:**
```
docker-compose -f docker-compose-dev.yml run --rm django python manage.py createsuperuser
```

**Зайти по адресу**
```
127.0.0.1:8000/admin
```

**Профилирование запросов**
```
127.0.0.1:8000/silk
```

**Запусков тестов**
```
./manage.py test --debug-mode
```
