FROM python:3.6
ENV PYTHONUNBUFFERED 1

RUN mkdir /config
ADD /config/requirements.txt /config/
RUN pip install -r /config/requirements.txt

RUN mkdir /src \
 && mkdir -p /var/todo_list/ \
 && mkdir -p /var/todo_list/static/ \
 && mkdir -p /var/todo_list/media/

COPY . .
WORKDIR /src
ENTRYPOINT ["/bin/docker/wait-for-postgres-start.sh"]
