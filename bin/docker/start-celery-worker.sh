#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

celery worker -A todo_list.celery
