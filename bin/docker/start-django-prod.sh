#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

./manage.py migrate
# ToDo: в данном проекте, возможно, не будет админки и статики
./manage.py collectstatic --noinput
gunicorn todo_list.wsgi:application -c /config/gunicorn.conf.py
