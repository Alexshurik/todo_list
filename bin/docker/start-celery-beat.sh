#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

celery beat -A todo_list.celery --pidfile=/tmp/myprojectbeat.pid -s /var/todo_list/celerybeat-schedule
